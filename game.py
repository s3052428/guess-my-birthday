from random import randint

player_name = input("Hi! What is your name? ")
month_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
              'August', 'September', 'August', 'September', 'October', 'November', 'December']

for guess_number in range(1, 6):
    month = randint(1, 12)
    year = randint(1924, 2004)

    print(
        f"Guess {guess_number} : {player_name}, were you born in {month_list[month - 1]}/{year}?")

    player_response = input('yes or no? ').lower()

    if player_response == 'yes':
        print("I knew it")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
        exit()
    else:
        print("Drat! Lemme try again!")
